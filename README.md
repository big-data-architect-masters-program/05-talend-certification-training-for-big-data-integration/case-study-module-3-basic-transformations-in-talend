# Case Study Module 3 Basic Transformations In Talend

## Problem 1 - Filtering Application

For a health insurance company, the data of all the applicants is present in a delimited file separated with semicolon `;`. 
Based on terms and conditions of the Agreement, these are the following constraints, an applicant should meet to be eligible for the insurance:

- Company will not cover the insurance if the age of a person is more than 70 years
- The minimum salary of the person should be more than 20000 dollars
- Country should be England only

An applicant who fulfills all the above criteria will only be accepted otherwise rejected

Create two separate files where:
- The first file should contain the list of accepted records sorted alphabetically, based on Last name
- The second file should contain the list of rejected records and with each record. An error message for each rejected record will display in the same table to explain why such a record has been rejected

**Input file:** `customer_details.csv`

## Solution 1:

- create metadata for input delimited, I named it `md_case_study_03`

  ![](screenshots/cs_01.png)

- create metadata for output delimited, I named it `metadata_case_study_3_output`

  ![](screenshots/cs_02.png)

- add `tFileInputDelimited` from metadata input `md_case_study_03`

  ![](screenshots/cs_03.png)

- add `tMap` and take the input from `tFileInputDelimited`
- configure output schema on `tMap` for: `accepted`,`rejected_by_age`,`rejected_by_salary`,`rejected_by_country`

  ![accepted](screenshots/cs_04.png)

  ![rejected_by_age](screenshots/cs_05.png)

  ![rejected_by_salary](screenshots/cs_06.png)

  ![rejected_by_country](screenshots/cs_07.png)

- add `tSortRow` which get input from `tMap/accepted` and output to `tFileOutputDelimited`

  ![](screenshots/cs_08.png)

- add 4 `tFileOutputDelimited` to write output file as CSV format

  ![accepted](screenshots/cs_09.png)

  ![rejected_by_age](screenshots/cs_10.png)

  ![rejected_by_salary](screenshots/cs_11.png)

  ![rejected_by_country](screenshots/cs_12.png)

- run the job and get the output files:

  ![full_job_view](screenshots/cs_13.png)

  - [accepted](output/accepted.csv)
  - [rejected_by_age](output/rejected_by_age.csv)
  - [rejected_by_salary](output/rejected_by_salary.csv)
  - [rejected_by_country](output/rejected_by_country.csv)

## Problem 2 - Combining heterogeneous data sources

It is always a challenge in an organization to combine data from different sources in a unique data platform with the required transformation on a data set. In a business requirement, client has a record maintained in csv file and a database table.
The client wants to maintain the record and database in such a way that:

- Update the gender and email of the existing records in database after comparing from the CSV file for valid email address
- Create a list of records containing invalid email address or the records which are missing in database

**Database table:** `customers`

**CSV record:** `customer.csv`

## Solution 2:

- create metadata to get data from MySQL database connection
- create metadata to capture csv file as input
- create new job
- add `tMysqlConnection`, `tMysqlInput`, `tFileInputDelimited` from metadata repository
- add `tJoin` and connect main row from `tMysqlInput`
- connect main row from `tFileInputDelimited` to `tJoin` as lookup
- configure `tJoin`

_to be continued..._

  ![](screenshots/cs_14.png)